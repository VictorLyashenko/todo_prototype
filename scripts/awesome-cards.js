'use strict';

function Card(config) {
	this.config = config;

	this.id = null;
	this.LS = localStorage;
	this.cardsData = this.LS.getItem('cards');

	this.parseCardsData = JSON.parse(this.cardsData);

	this._cardTitle = '';
	this._cardText = '';
	this._cardImportant = false;

	this._cardBlock = null;
	this._cardButton = null;
	this._titleEl = null;
	this._textEl = null;
	this._importantEl = null;
	this._deleteButtonEl = null;

	this._cardsBlock = null;
	this._changeTitleEl = null;
	this._changeTextEl = null;
	this._changeImportantEl = null;
	this._changeButtonEl = null;
};
Object.assign(Card.prototype, {

	_createElements : function () {
		this._cardBlock = document.createElement('div');
		this._cardButton = document.createElement('div');
		this._titleEl = document.createElement('h2');
		this._textEl = document.createElement('p');
		this._importantEl = document.createElement('input');
		this._deleteButtonEl = document.createElement('button');
		this._changeButtonEl = document.createElement('button');

		this._changeTitleEl = document.createElement('input');
		this._changeTextEl = document.createElement('input');
	},

	_addElements : function () {
		this._cardBlock.appendChild(this._titleEl);
		this._cardBlock.appendChild(this._changeTitleEl);
		this._cardBlock.appendChild(this._textEl);
		this._cardBlock.appendChild(this._changeTextEl);
		this._cardBlock.appendChild(this._importantEl);
		this._cardButton.appendChild(this._changeButtonEl);
		this._cardButton.appendChild(this._deleteButtonEl);

		this._cardsBlock.appendChild(this._cardBlock);
		this._cardBlock.appendChild(this._cardButton);
	},

	_addElementsContent : function () {
		this._cardBlock.classList.add('card-block');
		this._cardButton.classList.add('card-button');
		this._titleEl.textContent = this._cardTitle;
		this._textEl.textContent = this._cardText;
		this._deleteButtonEl.textContent = 'DELETE';
		this._changeButtonEl.textContent = 'CHANGE';
		this._changeButtonEl.setAttribute('data-action', 'change');

		this._importantEl.setAttribute('type', 'checkbox');
		this._importantEl.classList.add('important');
		if(this._cardImportant) {
			this._importantEl.setAttribute('checked', true);
		};

		this._changeTitleEl.style.display = 'none';
		this._changeTextEl.style.display = 'none';
	},

	_generateCard : function () {
		this._createElements();
		this._addElementsContent();
		this._addElements();
		this._saveState();
	},

	_saveState : function () {
		if(!this.config.id) {
			this._generateId();
			this._updateState();
		} else {
			this.id = this.config.id;
		};
	},

	_updateState : function () {
		if (this.cardsData !== null) {
			this.parseCardsData[this.id] = {
				titles: this._cardTitle,
				text: this._cardText,
				important: this._cardImportant
			};
			var strCardsData = JSON.stringify(this.parseCardsData);
			this.LS.setItem('cards', strCardsData);
		} else {
			this.parseCardsData = {};
			this.parseCardsData[this.id] = {
				titles: this._cardTitle,
				text: this._cardText,
				important: this._cardImportant
			};
			var strCardsData = JSON.stringify(this.parseCardsData);
			this.LS.setItem('cards', strCardsData);
		};
	},  

	_getData : function () {
		this._cardTitle = this.config.titles;
		this._cardText = this.config.text;
		this._cardImportant = this.config.important;
	},

	_changeCard : function () {
		this._titleEl.style.display = 'none';
		this._textEl.style.display = 'none';

		this._changeTitleEl.style.display = 'block';
		this._changeTextEl.style.display = 'block';

		this._changeTitleEl.value = this._cardTitle;
		this._changeTextEl.value = this._cardText;

		this._changeButtonEl.textContent = 'SAVE';
		this._changeButtonEl.setAttribute('data-action', 'save');
	},

	_saveCard : function () {

		this._titleEl.textContent = this._changeTitleEl.value;
		this._textEl.textContent = this._changeTextEl.value;

		this._cardTitle = this._changeTitleEl.value;
		this._cardText = this._changeTextEl.value;

		this._changeTitleEl.style.display = 'none';
		this._changeTextEl.style.display = 'none';

		this._titleEl.style.display = 'block';
		this._textEl.style.display = 'block';

		this._changeButtonEl.textContent = 'CHANGE';
		this._changeButtonEl.setAttribute('data-action', 'change');

		this._updateState();
	},

	_attachEvents : function () {
		var self = this;
		this._deleteButtonEl.addEventListener('click', function (event) {
			event.preventDefault();

			self._cardsBlock.removeChild(self._cardBlock);
			self.cardsData = self.LS.getItem('cards');
			self.parseCardsData = JSON.parse(self.cardsData);

			delete	self.parseCardsData[self.id];

			var strCardsData = JSON.stringify(self.parseCardsData);
			self.LS.setItem('cards', strCardsData);
		});

		this._changeButtonEl.addEventListener('click', function (event) {
			event.preventDefault();
			var action = self._changeButtonEl.getAttribute('data-action');

			switch (action) {
				case 'change':
					self._changeCard();
					break;
				case 'save':
					self._saveCard();
					break;
			};
		});

		this._importantEl.addEventListener('click', function (event) {
			 if (self._cardImportant) {
	        	self._cardImportant = false;
		    } else if (!self._cardImportant){
		        self._cardImportant = true;
		    };
		});
	},

	_generateId : function () {
		this.id = new Date().getTime();
	},

	init : function () {
		this._cardsBlock = document.querySelector('#cardsBlock');

		this._getData();
		this._generateCard();
		this._attachEvents();
	},
});





	

